import React from 'react';
import './App.css';
import Bio from './components/Bio';
import './components/style.css';

function App() {
  return (
    <div className="row">
      <div className="col-md-12">
        <Bio/>
      </div>
    </div>
  );
}

export default App;
