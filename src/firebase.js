import * as firebase from "firebase";

 var firebaseConfig = {
    apiKey: "AIzaSyDkDWnNxAqg3oc61mJgNqqceS-mPK-0n2E",
    authDomain: "react-log-5e43c.firebaseapp.com",
    databaseURL: "https://react-log-5e43c.firebaseio.com",
    projectId: "react-log-5e43c",
    storageBucket: "react-log-5e43c.appspot.com",
    messagingSenderId: "847855657451",
    appId: "1:847855657451:web:ceb5d6e5fc485ad45eb9ae"
  };
  // Initialize Firebase
  var fireDb = firebase.initializeApp(firebaseConfig);

  export default fireDb.database().ref();