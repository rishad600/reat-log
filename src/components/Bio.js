import React,{useState,useEffect} from 'react';
import BioForm from './BioForm';
import firebaseDb from "../firebase"

let firebaseData;

const Bio = () => {

    var[bioObjects,setBioObjects] = useState({})
    var [currentId,setCurrentId] = useState('')
    
    //retriving from firebase
    useEffect(()=>{
        //invokes whenever operation occur
        firebaseDb.child('bio').on('value',snapshot=>{
            if(snapshot.val()!=null)
            setBioObjects({
                ...snapshot.val()
            })
            else
            //update if value is null
            setBioObjects({})
            // console.log(newData.MChLnU8Zz4kbJK0Q9KK);
        })
    },[])

    const addOrEdit = obj=>{
        
        if (currentId === '')
            firebaseDb.child('bio').push(
                obj,
                err => {
                    if (err)
                    console.log(err)
                }
            )
        else
        firebaseDb.child(`bio/${currentId}`).set(
            obj,
            err => {
                if (err)
                console.log(err)
                else
                setCurrentId('')
            }
        )
    }

    const onDelete = key => {
        if (window.confirm('Are you Sure to delete')){
            firebaseDb.child(`bio/${key}`).remove(
                err => {
                    if (err)
                    console.log(err)
                    else
                    setCurrentId('')
                }
            )
        }
    }

    return (
        <>
        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <h1 class="display-4">Register</h1>
            </div>
        </div>
        <div className="container">
            <div className="row">
                <div className="col-md-12">
                    <BioForm {...({addOrEdit, currentId, bioObjects })} />
                </div>
                <div className="col-md-12">
                    <table className="table table-borderless">
                        <thead className="thead-light">
                            <tr>
                                <th>First name</th>
                                <th>Last name</th>
                                <th>Date of birth</th>
                                <th>Age</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                Object.keys(bioObjects).map(id => {
                                    var today = new Date().getFullYear();
                                    var birthYear = new Date(bioObjects[id].dateOfBirth).getFullYear();
                                    var ageNow = today - birthYear;
                                    console.log(ageNow);
                                    console.log(birthYear);
                                    console.log(today);
                                    return <tr>
                                        <td>{bioObjects[id].firstName}</td>
                                        <td>{bioObjects[id].lastName}</td>
                                        <td>{bioObjects[id].dateOfBirth}</td>
                                        <td>{ageNow}</td>
                                        <td className="buttonCol">
                                            <a className="btn text-primary" onClick={() => {setCurrentId(id)} }>
                                                <i className="fas fa-pencil-alt"></i>
                                            </a>
                                            <a className="btn text-danger" onClick={() => {onDelete(id)} }>
                                                <i className="fas fa-trash-alt"></i>
                                            </a>
                                        </td>
                                    </tr>
                                })
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </>
    );
}

export default Bio;