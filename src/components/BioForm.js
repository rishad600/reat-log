import React,{useState,useEffect} from 'react';
// import Datepicker from 'react-datepicker'
// import 'react-datepicker/dist/react-datepicker.css'
import './style.css';

const BioForm = (props) => {

    const initialFieldValues ={
        firstName: '',
        lastName: '',
        dateOfBirth: '',
        age: ''
    }

    var [values, setValues] = useState(initialFieldValues)

    //editing values
    useEffect(() => {
        //whenver change occur in currentid,bioobject 
       if(props.currentId =='')
            setValues({
                ...initialFieldValues
            })
       else
            setValues({
                ...props.bioObjects[props.currentId]
            })
    }, [props.currentId, props.bioObjects])


    //getting name and value from input
    const handleInputChange = e =>{
        var {name, value} = e.target

        setValues({
            ...values,
            [name]: value
        })
    }

    //submitting the values
    const handleFormSubmit = e =>{
        e.preventDefault();
        props.addOrEdit(values)
    }

    return (
        <form autoComplete="off" onSubmit={handleFormSubmit}>
            <div className="form-group input-group row">
            <label  class="col-4 col-form-label">First name</label>
            <div class="col-8">
                <input className="form-control" placeholder="First name" name="firstName" 
                value={values.firstName}
                onChange={handleInputChange}
                />
            </div>
            </div>
            <div className="form-group input-group row">
            <label  class="col-4 col-form-label">Last name</label>
            <div class="col-8">
                <input className="form-control" placeholder="Last name" name="lastName" 
                value={values.lastName}
                onChange={handleInputChange}
                />
            </div>
            </div>

            
            <div class="form-group input-group row">
            <label  class="col-4 col-form-label">Date of birth</label>
            <div class="col-8">
                <input class="form-control" type="date" data-date-format="mm/dd/yyyy" name="dateOfBirth" 
                value={values.dateOfBirth}
                onChange={handleInputChange}
                />
                {/* <Datepicker 
                value={values.dateOfBirth}
                dateFormat='dd/MM/YY'
                onChange={handleInputChange}
                /> */}
            </div>
            </div>

            <div className="form-group input-group row submitRow">
                <input type="submit" value={props.currentId==''?"save" : "update"} className="btn  btn-primary"/>
            </div>
            
        </form>
    );
}

export default BioForm